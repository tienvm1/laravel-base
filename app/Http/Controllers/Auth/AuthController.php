<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Service\JwtService;

class AuthController extends Controller
{
    public function login()
    {
        $jwtService = new JwtService();
        echo $jwtService->encode();
    }
}
