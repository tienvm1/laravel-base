<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\ProductModel;

class ProductController extends Controller
{
    public function home()
    {
        $productModel = new ProductModel();
        $products = $productModel->getAllProduct();
        dd($products);
    }
}
