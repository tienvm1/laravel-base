<?php

namespace App\Http\Middleware;

use App\Http\Service\JwtService;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Closure;
class JwtMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $token          = $request->header('token');
//        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zd2lmdHBvZC5vcmciLCJhdWQiOiJodHRwOlwvXC9zd2lmdHBvZC5vcmciLCJpYXQiOjEzNTY5OTk1MjQsIm5iZiI6MTM1NzAwMDAwMH0.zetqvOr7tgwPrbCUpuwcIbuOiLA1RcvVjeI2Ssqi2-Y';
        $jwtService     = new JwtService();
        $isLoginSuccess = $jwtService->decode($token);

        if($isLoginSuccess){
            return $next($request);
        }

        return response()->json(['error' => 'Not authorized.'],403);

    }
}
