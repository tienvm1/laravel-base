<?php

namespace App\Http\Service;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use phpDocumentor\Reflection\Types\False_;

class JwtService
{

    protected $algorithm;
    protected $key;
    protected $payload;

    public function __construct()
    {
        $this->algorithm = 'HS256';
        $this->key = env('APP_KEY');
        $this->payload = array(
            "iss" => "http://swiftpod.org",
            "aud" => "http://swiftpod.org",
            "iat" => 1356999524,
            "nbf" => 1357000000
        );
    }

    public function encode()
    {
        return JWT::encode($this->payload, $this->key, $this->algorithm);
    }

    public function decode($jwt)
    {
        try {
            return JWT::decode($jwt, new Key($this->key, $this->algorithm));
        }catch (\Exception $exception){
            return false;
        }
    }
}
