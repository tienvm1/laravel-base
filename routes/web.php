<?php

use App\Http\Controllers\Product\ProductController;
use \App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/get-token-jwt', [AuthController::class, 'login'])->name('login');

Route::middleware(['jwt'])->group(function () {

    Route::get('/product', [ProductController::class, 'home'])->name('product');

});
